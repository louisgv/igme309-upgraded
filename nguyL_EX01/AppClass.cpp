#include "AppClass.h"
using namespace Simplex;
void Application::InitVariables(void)
{
#pragma region DOES NOT NEED CHANGES
	/*
		This part initialize the camera position so I can see the scene better;
		the guide cube and the tip of the pencil so all solutions on the exam
		look the same to the grader
	*/
	m_pCameraMngr->SetPositionTargetAndUpward(AXIS_Z * 35.0f, ZERO_V3, AXIS_Y);

	m_pGuideCube = new MyMesh();
	m_pGuideCube->GenerateCube(10.0f, C_WHITE);

	m_pMesh = new MyMesh();
	m_pMesh->GeneratePencil(1.0f, 2.0f, 6.0f, 1.0f, 8, C_WHITE);
#pragma endregion

	//Please change to your name and email
	m_sProgramer = "Lab N - lab@mail.rit.edu";

	// Initialize and loop the SFX:
	m_soundBuffer.loadFromFile("writing.wav");

	m_sound.setBuffer(m_soundBuffer);

	m_sound.setLoop(true);

	m_sound.play();

	m_vectVertexList = m_pGuideCube->GetVertexList();

	m_pCurrentLocation = &(m_vectVertexList.at(m_iNextVertexIndex - 1));
	m_pNextLocation = &(m_vectVertexList.at(m_iNextVertexIndex));
}
void Application::Update(void)
{
#pragma region DOES NOT NEED CHANGES
	/*
		This updates the internal clock of the Simplex library and process the Arcball
	*/
	//Update the system so it knows how much time has passed since the last call
	m_pSystem->Update();

	//Is the arcball active?
	ArcBall();
#pragma endregion
}
void Application::Display(void)
{
	// Clear the screen
	ClearScreen();

	matrix4 m4Projection = m_pCameraMngr->GetProjectionMatrix();
	matrix4 m4View = m_pCameraMngr->GetViewMatrix();

	//Get a timer
	static float fTimer = 0;	//store the new timer
	static uint uClock = m_pSystem->GenClock(); //generate a new clock for that timer
	fTimer += m_pSystem->GetDeltaTime(uClock); //get the delta time for that timer


	float fPercentage = MapValue(fTimer, 0.0f, m_fTimeBetweenStops, 0.0f, 1.0f);

	//calculate the current position
	vector3 v3CurrentPos = glm::lerp(*m_pCurrentLocation, *m_pNextLocation, fPercentage);

	if (fPercentage >= 0.999f) {
		fTimer = 0;

		m_iNextVertexIndex = (m_iNextVertexIndex + 1) % m_vectVertexList.size();

		m_pCurrentLocation = m_pNextLocation;
		m_pNextLocation = &(m_vectVertexList.at(m_iNextVertexIndex));

		// Get the current drawing face, then assign the appropriate 
		// axis for rotation
		int iFaceIndex = m_iNextVertexIndex / 6;
		switch (iFaceIndex)
		{
		case 1:
			m_v3PencilRotateAxis = AXIS_Y;
			break;
		case 2:
			m_v3PencilRotateAxis = -AXIS_X;
			break;
		case 3:
			m_v3PencilRotateAxis = AXIS_Y;
			break;
		case 4:
			m_v3PencilRotateAxis = -AXIS_Y;
			break;
		case 5:
			m_v3PencilRotateAxis = AXIS_X;
			break;
		case 0:
		default:
			m_v3PencilRotateAxis = AXIS_Z;
			break;
		}

		// To avoid gimball lock, simply rotate around a different axis 
		// since we're always using identity as the base
		if (iFaceIndex == 3) {
			m_fRotateAmount = PI;
		}
		else {
			m_fRotateAmount = PI/2;
		}

		// std::cout << m_v3PencilRotateAxis.x << " " << m_v3PencilRotateAxis.y << " " << m_v3PencilRotateAxis.z << std::endl;
	}

	// Rotate around identity, then move to the current position
	matrix4 m4PencilModel = glm::translate(IDENTITY_M4, v3CurrentPos) * glm::rotate(IDENTITY_M4, m_fRotateAmount, m_v3PencilRotateAxis);

	// Apply the arcball to align with camera after rotation and translation are done:
	m_pMesh->Render(m4Projection, m4View, ToMatrix4(m_qArcBall) * m4PencilModel);

#pragma region DOES NOT NEED CHANGES
	/*
		This part does not need any changes at all, it is just for rendering the guide cube, the
		skybox as a frame of reference and the gui display
	*/
	//Render the guide cube
	m_pGuideCube->Render(m4Projection, m4View, ToMatrix4(m_qArcBall));

	// draw a skybox
	m_pMeshMngr->AddSkyboxToRenderList();
	//render list call
	m_uRenderCallCount = m_pMeshMngr->Render();
	//clear the render list
	m_pMeshMngr->ClearRenderList();

	//draw gui
	DrawGUI();

	//end the current frame (internally swaps the front and back buffers)
	m_pWindow->display();
#pragma endregion
}

void Application::Release(void)
{
	//release GUI
	ShutdownGUI();

	//I already deallocated the memory for the meshes if you allocate new memory please
	//deallocate it before ending the program
	SafeDelete(m_pMesh);
	SafeDelete(m_pGuideCube);

	m_pCurrentLocation = nullptr;
	m_pNextLocation = nullptr;
}